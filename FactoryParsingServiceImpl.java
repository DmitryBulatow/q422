package itis.parsing2;

import itis.parsing2.annotations.Concatenate;
import itis.parsing2.annotations.NotBlank;

import java.io.*;
import java.lang.reflect.Field;
import java.nio.file.DirectoryStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FactoryParsingServiceImpl implements FactoryParsingService {

	@Override
	public Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException {
		List<File> files = readFiles(factoryDataDirectoryPath);

		Map<String, String> inf = readInformation(files);

		Class clazz = null;
		Factory factory = new Factory();
		List<FactoryParsingException.FactoryValidationError> errors = new ArrayList<>();

		try {
			clazz = Class.forName(Factory.class.getName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		for (Field declaredField : clazz.getDeclaredFields()) {
			declaredField.setAccessible(true);

			if (declaredField.getAnnotation(NotBlank.class) != null) {

				try {
					if (!inf.get(declaredField.getName()).equals("") &&
							inf.get(declaredField.getName()) != null) {
						declaredField.set(factory, inf.get(declaredField.getName()));
					} else {
						errors.add(new FactoryParsingException
								.FactoryValidationError(declaredField.getName(), "Not Blank"));
					}

				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			} else if (declaredField.getAnnotation(Concatenate.class) != null) {
				String delimiter = declaredField.getAnnotation(Concatenate.class).delimiter();
				String[] strings = declaredField.getAnnotation(Concatenate.class).fieldNames();
				StringBuilder toSet = new StringBuilder();
				for (String string : strings) {
					toSet.append(inf.get(string) == null ? "" : inf.get(string) + delimiter);
				}
				try {
					declaredField.set(factory, toSet.toString());
				} catch (IllegalAccessException e) {
					e.printStackTrace();
				}
			} else {
				System.out.println(declaredField.getType().getTypeName());
				System.out.println(List.class.getTypeName());
				if (declaredField.getType().getTypeName().equals(List.class.getTypeName())){
					List<String> t = new ArrayList<>();
					for (String s : inf.get(declaredField.getName()).replace("[", "")
							.replace("]", "")
							.split(",")) {
						t.add(s);
					}
					try {
						declaredField.set(factory, t);
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
				}
				else {
					try {
						declaredField.set(factory, inf.get(declaredField.getName()));
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}
				}
			}
		}
		if (errors.size() > 0) {
			throw new FactoryParsingException("notail", errors);
		}
		return factory;
	}

	private Map<String, String> readInformation(List<File> files) {
		Map<String, String> map = new HashMap<>();
		for (File file : files) {
			try {
				BufferedReader br = new BufferedReader(
						new InputStreamReader(
								new FileInputStream(file)));
				String s = br.readLine();
				while (s != null) {
					if (s.contains(":")) {
						if (s.split(":").length == 1) {
							map.put(s.split(":")[0].replace("\"", ""), "");

						} else if (!s.split(":")[1].replace("\"", "").equals("null")) {
							map.put(s.split(":")[0].replace("\"", ""),
									s.split(":")[1].replace("\"", ""));
						} else {
							map.put(s.split(":")[0].replace("\"", ""),
									null);
						}
					}
					s = br.readLine();
				}

			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		return map;
	}

	private List<File> readFiles(String factoryDataDirectoryPath) {
		File file = new File(factoryDataDirectoryPath);
		List files = new ArrayList();

		for (File f : file.listFiles()) {
			if (f.isFile()) {
				files.add(f);
			}
		}
		return files;
	}
}
